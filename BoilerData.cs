﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Hef3MS.Sensors.HefSensors;
using Hef3MS.Sensors;

namespace Hef3MS.Data
{

    public class BoilerData2
    {
        public BoilerData2()
        {
            FeedWater = new FeedWaterSensor();
            Steam = new SteamSensor();

            ColdAirTemperature = new TemperatureSensor();
            AirBeforeHeaterTemperature = new TemperatureSensor();
            HotAirTemperature = new TemperatureSensor();

            WasteGasesTemperature = new TemperatureSensor();
        }

        public FeedWaterSensor FeedWater { get; set; }
        public SteamSensor Steam { get; set; }

        public TemperatureSensor AirBeforeHeaterTemperature { get; set; }
        public TemperatureSensor ColdAirTemperature { get; set; }
        public TemperatureSensor HotAirTemperature { get; set; }

        public TemperatureSensor WasteGasesTemperature { get; set; }

        public double StartStopCount { get; set; }
        public double WorkingHoursCount { get; set; }

        public double steamEnthalpy1 { get; set; }
        public double steamEnthalpy2 { get; set; }
        public double steamEnthalpy3 { get; set; }

        public double heatGrossValue { get; set; }
    }

}
