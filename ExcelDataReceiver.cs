﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Hef3MS.Data;
using Hef3MS.Scaner.XLS.Properties;
using Microsoft.Office.Interop.Excel;

namespace Hef3MS.Scaner.XLS
{
    public class ExcelDataReceiver
    {
        private static ExcelDataReceiver m_instance = null;

        public static ExcelDataReceiver Instance
        {
            get
            {
                if (null == m_instance)
                {
                    m_instance = new ExcelDataReceiver();
                }

                return m_instance;
            }
        }

        private ExcelDataReceiver()
        {
        }

        private const int BOILER_COUNT = 3;

        public Hef3Data2 ReceiveData(DateTime adate)
        {
            Microsoft.Office.Interop.Excel.Application app = new Microsoft.Office.Interop.Excel.Application();

            Hef3Data2 data = new Hef3Data2();

            //TODO: Refactor this long method

            try
            {
                ReceiveBoilersData(ref adate, app, ref data);
            }
            catch (Exception e)
            {
                SystemLog.Write("Boilers data receive error:" + e.Message + "\n" + e.StackTrace, EventLogEntryType.Error);
            }

            try
            {
                ReceiveTurbinesData(ref adate, app, ref data);
            }
            catch (Exception e1)
            {
                SystemLog.Write("Turbines data receive error:" + e1.Message + "\n" + e1.StackTrace, EventLogEntryType.Error);
            }

            try
            {
                app.Quit();

                Marshal.ReleaseComObject(app);

                app = null;
            }
            catch (Exception e2)
            {
                SystemLog.Write("On excel closure error:" + e2.Message + "\n" + e2.StackTrace, EventLogEntryType.Error);
            }

            return data;
        }

        private Hef3Data2 ReceiveBoilersData(ref DateTime adate, Microsoft.Office.Interop.Excel.Application app, ref Hef3Data2 data)
        {
            object missing = Type.Missing;

            Workbook wbk = app.Workbooks.Open(Settings.Default.Path + Settings.Default.BoilerFile, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing);

            app.EnableEvents = false;

            FillBoilersFromWorkbook(adate, wbk, data);

            wbk.Close(false);

            Marshal.ReleaseComObject(wbk);
            wbk = null;

            return data;
        }

        private Hef3Data2 ReceiveTurbinesData(ref DateTime adate, Microsoft.Office.Interop.Excel.Application app, ref Hef3Data2 data)
        {
            object missing = Type.Missing;

            Workbook wbk = app.Workbooks.Open(Settings.Default.Path + Settings.Default.TurbineFile, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing, missing);

            app.EnableEvents = false;

            FillTurbinesFromWorkbook(adate, wbk, data);

            wbk.Close(false);

            Marshal.ReleaseComObject(wbk);
            wbk = null;

            return data;
        }

        private const int TURBINES_COUNT = 2;

        private void FillTurbinesFromWorkbook(DateTime adate, Workbook wbk, Hef3Data2 data)
        {
            for (int i = 1; i <= TURBINES_COUNT; i++)
            {
                Worksheet sheet = (Worksheet)wbk.Sheets[i];

                ActiveSheet = sheet;

                Range result = FindTableStartFor(adate);

                if (null != result)
                {
                    Range dayRange = GetRowRange(result.Row + adate.Day - 1);

                    data.Turbines[i - 1] = GetTurbineDataFromRow(dayRange);
                }
                else
                {
                    SystemLog.Write(String.Format("Turbine {0} day row for {1} is not found", i, adate), EventLogEntryType.Warning);
                }
            }
        }

        private TurbineData GetTurbineDataFromRow(Range row)
        {
            TurbineData data = new TurbineData();

            if (null == row)
            {
                return data;
            }

            if (null == row.Value2)
            {
                return data;
            }

            object[,] values = (object[,])row.Value2;

            data.Temperature.Value = GetDoubleValue(values[1, 2]);
            data.Pressure.Value = GetDoubleValue(values[1, 3]);

            data.FlowRate.ByDevice.Value = GetDoubleValue(values[1, 4]);
            data.FlowRate.Actual.Value = GetDoubleValue(values[1, 5]);

            data.ExhaustSteam.Pressure.Value = GetDoubleValue(values[1, 6]);
            data.POSteam.Pressure.Value = GetDoubleValue(values[1, 7]);
            data.TOSteam.Pressure.Value = GetDoubleValue(values[1, 8]);

            data.ExhaustSteam.Temperature.Value = GetDoubleValue(values[1, 9]);
            data.POSteam.Temperature.Value = GetDoubleValue(values[1, 10]);

            data.CirculateWater.IncomingTemperature.Value = GetDoubleValue(values[1, 11]);
            data.CirculateWater.OutgoingTemperature.Value = GetDoubleValue(values[1, 12]);

            data.WorkingHours = (int)GetDoubleValue(values[1, 13]);
            data.StartStopCount = (int)GetDoubleValue(values[1, 14]);

            return data;
        }

        private void FillBoilersFromWorkbook(DateTime adate, Workbook wbk, Hef3Data2 data)
        {
            for (int i = 1; i <= BOILER_COUNT; i++)
            {
                Worksheet sheet = (Worksheet)wbk.Sheets[i];

                ActiveSheet = sheet;

                Range result = FindTableStartFor(adate);

                if (null != result)
                {
                    Range dayRange = GetRowRange(result.Row + adate.Day - 1);

                    data.Boilers[i - 1] = GetBoilerDataFromRow(dayRange);
                }
                else
                {
                    SystemLog.Write(String.Format("Boiler {0} day row for {1} is not found", i, adate), EventLogEntryType.Warning);
                }
            }
        }

        private BoilerData2 GetBoilerDataFromRow(Range row)
        {
            BoilerData2 data = new BoilerData2();

            if (null == row)
            {
                return data;
            }

            if (null == row.Value2)
            {
                return data;
            }

            object[,] values = (object[,])row.Value2;

            data.Steam.Temperature.Value = GetDoubleValue(values[1, 2]);
            data.Steam.Pressure.Value = GetDoubleValue(values[1, 3]);
            data.Steam.FlowRate.ByDevice.Value = GetDoubleValue(values[1, 4]);
            data.Steam.FlowRate.Actual.Value = GetDoubleValue(values[1, 5]);

            data.ColdAirTemperature.Value = GetDoubleValue(values[1, 6]);
            data.AirBeforeHeaterTemperature.Value = GetDoubleValue(values[1, 7]);
            data.HotAirTemperature.Value = GetDoubleValue(values[1, 8]);

            data.WasteGasesTemperature.Value = GetDoubleValue(values[1, 9]);

            data.FeedWater.Temperature.Value = GetDoubleValue(values[1, 10]);
            data.FeedWater.Pressure.Value = GetDoubleValue(values[1, 11]);
            data.FeedWater.FlowRate.ByDevice.Value = GetDoubleValue(values[1, 12]);
            data.FeedWater.FlowRate.Actual.Value = GetDoubleValue(values[1, 13]);

            data.WorkingHoursCount = GetDoubleValue(values[1, 14]);
            data.StartStopCount = GetDoubleValue(values[1, 15]);

            data.steamEnthalpy1 = GetDoubleValue(values[1, 16]);
            data.steamEnthalpy2 = GetDoubleValue(values[1, 17]);
            data.steamEnthalpy3 = GetDoubleValue(values[1, 18]);

            data.heatGrossValue = GetDoubleValue(values[1, 20]);

            return data;
        }

        private double GetDoubleValue(object value)
        {
            if (value is double)
            {
                return (double)value;
            }

            return 0.0F;
        }

        private Worksheet ActiveSheet { get; set; }

        private Range FindTableStartFor(DateTime adate)
        {
            int regionStart = 1, regionSize = 30;

            Range result = default(Range);

            while ((!IsEndOfData(regionStart, regionSize)) && (!IsFound(result)))
            {
                result = FindTableStartInRegion(regionStart, regionSize, adate);

                regionStart += regionSize;
            }

            return result;
        }

        private Range FindTableStartInRegion(int regionStart, int regionSize, DateTime adate)
        {
            Range monthItem, dateItem;

            monthItem = FindMonthItem(regionStart, regionSize, adate);

            if (IsFound(monthItem))
            {
                dateItem = FindDateCellForMonth(monthItem, adate, regionStart, regionSize);
                return GetTableStart(dateItem);
            }

            return null;
        }

        private Range GetTableStart(Range dateItem)
        {
            if (IsFound(dateItem))
            {
                return SingleCellRange(TableStartCellName(dateItem.Row + 3));
            }

            return default(Range);
        }

        private Range SingleCellRange(string cellName)
        {
            return ActiveSheet.get_Range(cellName, cellName);
        }

        private string TableStartCellName(int tableStartRow)
        {
            return "A" + tableStartRow.ToString();
        }

        private Range FindDateCellForMonth(Range monthItem, DateTime adate, int regionStart, int regionSize)
        {
            if (!IsFound(monthItem))
                return default(Range);

            if (IsFound(FindYearItemOn(monthItem.Row, adate)))
            {
                return FindDateCellFor(monthItem.Row, regionSize);
            }
            else
            {
                //return FindDateCellInRegionRemains(monthItem, regionStart, regionSize, adate);
                return null;//default(Range);
            }
        }

        private Range FindDateCellInRegionRemains(Range monthItem, int regionStart, int regionSize, DateTime adate)
        {
            bool isStartFound = false;
            Range dateItem = default(Range);
            Range nextMonthItem;

            do
            {
                nextMonthItem = FindNextMonthItem(monthItem, regionStart, regionSize);

                if (IsFound(nextMonthItem))
                {
                    if (IsFound(FindYearItemOn(nextMonthItem.Row, adate)))
                    {
                        dateItem = FindDateCellFor(nextMonthItem.Row, regionSize);
                    }
                }
                else
                {
                    break;
                }

                isStartFound = IsFound(dateItem);
            } while ((nextMonthItem.Row != monthItem.Row) && (!isStartFound));

            return dateItem;
        }

        private Range FindNextMonthItem(Range monthItem, int regionStart, int regionSize)
        {
            Range monthRange = MonthRegionRange(regionStart, regionSize);
            return monthRange.FindNext(monthItem);
        }

        private Range FindDateCellFor(int monthRow, int regionSize)
        {
            return FirstColumnRange(monthRow, regionSize).Find("Дата", Type.Missing, XlFindLookIn.xlValues, Type.Missing, Type.Missing, XlSearchDirection.xlNext, Type.Missing, Type.Missing, Type.Missing);
        }

        private Range FirstColumnRange(int monthRow, int regionSize)
        {
            return ActiveSheet.get_Range("A" + monthRow.ToString(), "A" + (monthRow + regionSize).ToString());
        }

        private Range FindYearItemOn(int monthRow, DateTime adate)
        {
            Range yearRange = GetRowRange(monthRow);
            Range result = yearRange.Find(adate.Year, Type.Missing, XlFindLookIn.xlValues, XlLookAt.xlPart, XlSearchOrder.xlByRows, XlSearchDirection.xlNext, false, Type.Missing, Type.Missing);

            return result;
        }

        private Range GetRowRange(int monthRow)
        {
            Range yearRange = ActiveSheet.get_Range("A" + monthRow.ToString(), "Z" + monthRow.ToString());
            return yearRange;
        }

        private Range FindMonthItem(int regionStart, int regionSize, DateTime adate)
        {
            Range monthRange = MonthRegionRange(regionStart, regionSize);

            return monthRange.Find(MonthName2(adate), Type.Missing, XlFindLookIn.xlValues, XlLookAt.xlPart, XlSearchOrder.xlByRows, XlSearchDirection.xlNext, false, Type.Missing, Type.Missing);
        }

        private string MonthName2(DateTime adate)
        {
            switch (adate.Month)
            {
                case 1: return "Январь";
                case 2: return "Февраль";
                case 3: return "Март";
                case 4: return "Апрель";

                case 5: return "Май";
                case 6: return "Июнь";
                case 7: return "Июль";
                case 8: return "Август";

                case 9: return "Сентябрь";
                case 10: return "Октябрь";
                case 11: return "Ноябрь";
                case 12: return "Декабрь";
            }

            return "";
        }

        private Range MonthRegionRange(int regionStart, int regionSize)
        {
            return ActiveSheet.get_Range("E" + regionStart.ToString(), "E" + (regionStart + regionSize).ToString());
        }

        private bool IsFound(Range result)
        {
            if (null == result)
                return false;

            return !result.Equals(default(Range));
        }

        private bool IsEndOfData(int start, int size)
        {
            string rangeName;

            for (int index = start; index < size + start; index++)
            {
                rangeName = "A" + index.ToString();

                Range tempRange = ActiveSheet.get_Range(rangeName, rangeName);

                if (null != tempRange.Value2)
                    if ("" != tempRange.Value2.ToString())
                        return false;
            }

            return true;
        }
    }
}