﻿using System;
using System.Globalization;
using Hef3MS.Data;
using Hef3MS.Scaner.XLS.Properties;
using MySql.Data.MySqlClient;

namespace Hef3MS.Scaner.XLS
{
    class Hef3DBDataStorer
    {
        #region Boiler bases

        const int BOILER_BASE = 10;
        const int BOILER_SHIFT = 10;

        #endregion Boiler bases

        #region Turbine bases

        const int TURBINE_BASE = 50;
        const int TURBINE_SHIFT = 20;

        #endregion Turbine bases

        #region Boiler parms

        const int BOILER_STEAM_SENSOR = 1;
        const int BOILER_AIR_SENSORS = 2;
        const int BOILER_GAS_SENSORS = 3;
        const int BOILER_FEED_WATER_SENSORS = 4;
        const int BOILER_MISC_PARAMS = 5;

        #endregion Boiler parms

        #region Turbine parms

        const int TURBINE_TEMPERATURE_SENSOR = 1;
        const int TURBINE_PRESSURE_SENSOR = 2;
        const int TURBINE_FLOWRATE_SENSOR = 3;
        const int TURBINE_EXHAUST_STEAM_SENSOR = 4;
        const int TURBINE_PO_STEAM_SENSOR = 5;
        const int TURBINE_TO_STEAM_SENSOR = 6;
        const int TURBINE_CIRCULATE_WATER_SENSOR = 7;
        const int TURBINE_MISC_PARAMS = 8;

        #endregion Turbine parms

        #region Common turbine parms

        const int TEMPERATURE = 1;
        const int PRESSURE = 1;

        const int FLOWRATE_ACTUAL = 1;
        const int FLOWRATE_BYDEVICE = 2;

        const int WATER_TEMPERATURE_INCOMING = 1;
        const int WATER_TEMPERATURE_OUTGOING = 2;

        #endregion Common turbine parms

        #region Common boiler parms

        const int STEAM_TEMPERATURE = 1;
        const int STEAM_PRESSURE = 2;
        const int STEAM_FLOWRATE_BYDEVICE = 3;
        const int STEAM_FLOWRATE_ACTUAL = 4;
        const int STEAM_ENTALPY1 = 5;
        const int STEAM_ENTALPY2 = 6;
        const int STEAM_ENTALPY3 = 7;

        const int COLD_AIR_TEMPERATURE = 1;
        const int AIR_BEFORE_HEATER_TEMPERATURE = 2;
        const int HOT_AIR_TEMPERATURE = 3;

        const int WASTE_GASES_TEMPERATURE = 1;

        const int FEED_WATER_TEMPERATURE = 1;
        const int FEED_WATER_PRESSURE = 2;
        const int FEED_WATER_FLOWRATE_BYDEVICE = 3;
        const int FEED_WATER_FLOWRATE_ACTUAL = 4;

        const int WORKING_HOURS_COUNT = 1;
        const int START_STOP_COUNT = 2;
        const int HEAT_GROSS_VALUE = 3;

        #endregion Common boiler parms

        private Hef3DBDataStorer()
        {
        }

        private static Hef3DBDataStorer m_instance = null;

        public static Hef3DBDataStorer Instance
        {
            get
            {
                if (null == m_instance)
                {
                    m_instance = new Hef3DBDataStorer();
                }

                return m_instance;
            }
        }

        private static class SQLStrings
        {
            public static string Base { get { return "insert into hef3id (DateInput, hkid) values ( '{0:yyyy-MM-dd}', {1} )"; } }

            public static string Value { get { return "insert into idvalue (hid, kid, value) values ( {0}, {1}, {2} )"; } }

            public static string MaxIdSQL { get { return "select max(id) from hef3id"; } }
        }

        private MySqlConnection m_connection = null;

        public MySqlConnection Connection
        {
            get
            {
                if (null == m_connection)
                {
                    MySqlConnectionStringBuilder sb = new MySqlConnectionStringBuilder();
                    sb.Server = Settings.Default.Server;
                    sb.Database = Settings.Default.Database;

                    sb.UserID = Settings.Default.UserID;
                    sb.Password = Settings.Default.Password;

                    m_connection = new MySqlConnection(sb.ToString());
                    m_connection.Open();
                }

                return m_connection;
            }
        }

        public void SaveDataToDB(Hef3Data2 data)
        {
            MySqlCommand cmd = new MySqlCommand(SQLStrings.MaxIdSQL, Connection);

            object cmdRes = cmd.ExecuteScalar();

            int? maxId = 0;

            if (cmdRes != DBNull.Value)
                maxId = (int?)cmdRes;

            if (!maxId.HasValue)
                throw new InvalidOperationException("Unable to execute scalar on sql server!");

            maxId++;

            maxId = StoreBoilersData(data, maxId.Value);

            maxId = StoreTurbinesData(data, maxId.Value);
        }

        private int StoreTurbinesData(Hef3Data2 data, int maxId)
        {
            for (int i = 0; i < data.Turbines.Count; i++)
            {
                int delta = TURBINE_BASE + TURBINE_SHIFT * i;

                TurbineData turbine = data.Turbines[i];

                maxId = StoreTurbineCirculateWaterSensorData(maxId, delta, turbine);

                maxId = StoreTurbineExhaustSteamSensorData(maxId, delta, turbine);

                maxId = StoreTurbineFlowrateSensorData(maxId, delta, turbine);

                maxId = StoreTurbineMiscParamsData(maxId, delta, turbine);

                maxId = StoreTurbinePOSteamSensorData(maxId, delta, turbine);

                maxId = StoreTurbinePressureSensorData(maxId, delta, turbine);

                maxId = StoreTurbineTemperatureSensorData(maxId, delta, turbine);

                maxId = StoreTurbineTOSteamSensorData(maxId, delta, turbine);
            }
            return maxId;
        }

        private int StoreTurbineTOSteamSensorData(int maxId, int delta, TurbineData turbine)
        {
            AddHef3IdRow(TURBINE_TO_STEAM_SENSOR + delta);

            AddValueIdRow(maxId, STEAM_TEMPERATURE, turbine.TOSteam.Temperature.Value);
            AddValueIdRow(maxId, STEAM_PRESSURE, turbine.TOSteam.Pressure.Value);

            maxId += 1;
            return maxId;
        }

        private int StoreTurbineTemperatureSensorData(int maxId, int delta, TurbineData turbine)
        {
            AddHef3IdRow(TURBINE_TEMPERATURE_SENSOR + delta);

            AddValueIdRow(maxId, TEMPERATURE, turbine.Temperature.Value);

            maxId += 1;

            return maxId;
        }

        private int StoreTurbinePressureSensorData(int maxId, int delta, TurbineData turbine)
        {
            AddHef3IdRow(TURBINE_PRESSURE_SENSOR + delta);

            AddValueIdRow(maxId, PRESSURE, turbine.Pressure.Value);

            maxId += 1;

            return maxId;
        }

        private int StoreTurbinePOSteamSensorData(int maxId, int delta, TurbineData turbine)
        {
            AddHef3IdRow(TURBINE_PO_STEAM_SENSOR + delta);

            AddValueIdRow(maxId, STEAM_TEMPERATURE, turbine.POSteam.Temperature.Value);
            AddValueIdRow(maxId, STEAM_PRESSURE, turbine.POSteam.Pressure.Value);

            maxId += 1;

            return maxId;
        }

        private int StoreTurbineMiscParamsData(int maxId, int delta, TurbineData turbine)
        {
            AddHef3IdRow(TURBINE_MISC_PARAMS + delta);

            AddValueIdRow(maxId, WORKING_HOURS_COUNT, turbine.WorkingHours);
            AddValueIdRow(maxId, START_STOP_COUNT, turbine.StartStopCount);

            maxId += 1;

            return maxId;
        }

        private int StoreTurbineFlowrateSensorData(int maxId, int delta, TurbineData turbine)
        {
            AddHef3IdRow(TURBINE_FLOWRATE_SENSOR + delta);

            AddValueIdRow(maxId, FLOWRATE_ACTUAL, turbine.FlowRate.Actual.Value);
            AddValueIdRow(maxId, FLOWRATE_BYDEVICE, turbine.FlowRate.ByDevice.Value);

            maxId += 1;
            return maxId;
        }

        private int StoreTurbineExhaustSteamSensorData(int maxId, int delta, TurbineData turbine)
        {
            AddHef3IdRow(TURBINE_EXHAUST_STEAM_SENSOR + delta);

            AddValueIdRow(maxId, STEAM_TEMPERATURE, turbine.ExhaustSteam.Temperature.Value);
            AddValueIdRow(maxId, STEAM_PRESSURE, turbine.ExhaustSteam.Pressure.Value);

            maxId += 1;
            return maxId;
        }

        private int StoreTurbineCirculateWaterSensorData(int maxId, int delta, TurbineData turbine)
        {
            AddHef3IdRow(TURBINE_CIRCULATE_WATER_SENSOR + delta);

            AddValueIdRow(maxId, WATER_TEMPERATURE_INCOMING, turbine.CirculateWater.IncomingTemperature.Value);
            AddValueIdRow(maxId, WATER_TEMPERATURE_OUTGOING, turbine.CirculateWater.OutgoingTemperature.Value);

            maxId += 1;
            return maxId;
        }

        private int StoreBoilersData(Hef3Data2 data, int maxId)
        {
            for (int i = 0; i < data.Boilers.Count; i++)
            {
                int delta = BOILER_SHIFT * i + BOILER_BASE;

                BoilerData2 boiler = data.Boilers[i];

                maxId = StoreSteamSensorParms(maxId, delta, boiler);

                maxId = StoreAirSensorParms(maxId, delta, boiler);

                maxId = StoreFeedWaterSensorParms(maxId, delta, boiler);

                maxId = StoreGasSensorParms(maxId, delta, boiler);

                maxId = StoreBoilerMiscParms(maxId, delta, boiler);
            }
            return maxId;
        }

        private int StoreBoilerMiscParms(int maxId, int delta, BoilerData2 boiler)
        {
            AddHef3IdRow(BOILER_MISC_PARAMS + delta);

            AddValueIdRow(maxId, WORKING_HOURS_COUNT, boiler.WorkingHoursCount);
            AddValueIdRow(maxId, START_STOP_COUNT, boiler.StartStopCount);
            AddValueIdRow(maxId, HEAT_GROSS_VALUE, boiler.heatGrossValue);

            maxId += 1;
            return maxId;
        }

        private int StoreGasSensorParms(int maxId, int delta, BoilerData2 boiler)
        {
            AddHef3IdRow(BOILER_GAS_SENSORS + delta);

            AddValueIdRow(maxId, WASTE_GASES_TEMPERATURE, boiler.WasteGasesTemperature.Value);

            maxId += 1;

            return maxId;
        }

        private int StoreFeedWaterSensorParms(int maxId, int delta, BoilerData2 boiler)
        {
            AddHef3IdRow(BOILER_FEED_WATER_SENSORS + delta);

            AddValueIdRow(maxId, FEED_WATER_FLOWRATE_ACTUAL, boiler.FeedWater.FlowRate.Actual.Value);
            AddValueIdRow(maxId, FEED_WATER_FLOWRATE_BYDEVICE, boiler.FeedWater.FlowRate.ByDevice.Value);
            AddValueIdRow(maxId, FEED_WATER_PRESSURE, boiler.FeedWater.Pressure.Value);
            AddValueIdRow(maxId, FEED_WATER_TEMPERATURE, boiler.FeedWater.Temperature.Value);

            maxId += 1;
            return maxId;
        }

        private int StoreAirSensorParms(int maxId, int delta, BoilerData2 boiler)
        {
            AddHef3IdRow(BOILER_AIR_SENSORS + delta);

            AddValueIdRow(maxId, AIR_BEFORE_HEATER_TEMPERATURE, boiler.AirBeforeHeaterTemperature.Value);
            AddValueIdRow(maxId, COLD_AIR_TEMPERATURE, boiler.ColdAirTemperature.Value);
            AddValueIdRow(maxId, HOT_AIR_TEMPERATURE, boiler.HotAirTemperature.Value);

            maxId += 1;

            return maxId;
        }

        private int StoreSteamSensorParms(int maxId, int delta, BoilerData2 boiler)
        {
            AddHef3IdRow(BOILER_STEAM_SENSOR + delta);

            AddValueIdRow(maxId, STEAM_ENTALPY1, boiler.steamEnthalpy1);
            AddValueIdRow(maxId, STEAM_ENTALPY2, boiler.steamEnthalpy2);
            AddValueIdRow(maxId, STEAM_ENTALPY3, boiler.steamEnthalpy3);

            AddValueIdRow(maxId, STEAM_FLOWRATE_ACTUAL, boiler.Steam.FlowRate.Actual.Value);
            AddValueIdRow(maxId, STEAM_FLOWRATE_BYDEVICE, boiler.Steam.FlowRate.ByDevice.Value);

            AddValueIdRow(maxId, STEAM_PRESSURE, boiler.Steam.Pressure.Value);
            AddValueIdRow(maxId, STEAM_TEMPERATURE, boiler.Steam.Temperature.Value);

            maxId += 1;
            return maxId;
        }

        private void AddValueIdRow(int maxId, int valId, double value)
        {
            MySqlCommand cmdValue1 = new MySqlCommand(
                                    String.Format(
                                        SQLStrings.Value,
                                        maxId,
                                        valId,
                                        value.ToString("###0.0#", CultureInfo.InvariantCulture)
                                    ),
                                    Connection
                                );

            cmdValue1.ExecuteNonQuery();
        }

        private void AddHef3IdRow(int param)
        {
            MySqlCommand cmdBase = new MySqlCommand(
                                    String.Format(
                                        SQLStrings.Base,
                                        CurrentDate,
                                        param
                                    ),
                                    Connection
                                );

            cmdBase.ExecuteNonQuery();
        }

        public DateTime CurrentDate { get; set; }
    }
}