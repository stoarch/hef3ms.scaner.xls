﻿
using System;
namespace Hef3MS.Sensors.HefSensors
{
    public sealed class HEFSensorUID
    {
        public static Guid CirculateWater{ get { return new Guid("f14df798-419f-4d4a-abeb-445902b14d0a"); } }
        public static Guid FeedWater { get { return new Guid("bece421b-c4d9-4de8-93b3-cde181b3ee5d"); } }
        public static Guid Steam { get { return new Guid("8a19e3ba-cf5d-4a58-9d04-571758ecacf2"); } }
    }


    public class FeedWaterSensor : MediumSensor
    {
        public FeedWaterSensor()
        {
            Caption = SensorCaption.FeedWater;
        }
    }

    public class SteamSensor : MediumSensor
    {
        public SteamSensor()
        {
            Caption = SensorCaption.Steam;
        }
    }

    public class CirculateWaterSensor : ComplexSensor 
    {
        public CirculateWaterSensor()
        {
            UID = HEFSensorUID.CirculateWater;

            Sensors.Add(new TemperatureSensor(SensorKindCaption.Incoming));
            Sensors.Add(new TemperatureSensor(SensorKindCaption.Outgoing));
        }

        public TemperatureSensor IncomingTemperature
        {
            get
            {
                return (TemperatureSensor)this[SensorKindCaption.Incoming + SensorParameterCaption.Temperature ];
            }
        }

        public TemperatureSensor OutgoingTemperature
        {
            get
            {
                return (TemperatureSensor)this[SensorKindCaption.Outgoing + SensorParameterCaption.Temperature];
            }
        }
    } 

}