﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using Hef3MS.Scaner.XLS.Properties;
using MySql.Data.MySqlClient;

namespace Hef3MS.Scaner.XLS
{
    public partial class Hef3ScanerMainForm : Form, IDisposable
    {
        enum FillWeekEnd
        {
            Ignore,
            ProcessWeekend
        }

        private Object c_missing = Type.Missing;

        public Hef3ScanerMainForm()
        {
            CurrentDate = DateTime.Now;

            InitializeComponent();

            SystemLog.OnWrite += new OnWriteEventHandler(SystemLog_OnWrite);

            startOfPeriod_dateTimePicker.Value = CurrentDate.AddDays(-1);
            endOfPeriod_dateTimePicker.Value = CurrentDate;
        }

        public bool ForceExecution { get; set; }

        public bool QuitAfterExecution { get; set; }

        void SystemLog_OnWrite(string message, EventLogEntryType kind)
        {
            log_textBox.AppendText(DateTime.Now.ToString() + " ::" + message + "\n");
        }

        public DateTime StartDate { get { return startOfPeriod_dateTimePicker.Value; } }

        public DateTime EndDate { get { return endOfPeriod_dateTimePicker.Value; } }

        private void getDataButton_Click(object sender, EventArgs e)
        {
            GetDataForPeriod();
        }

        private void GetDataForPeriod()
        {
            CurrentDate = StartDate;

            progressBar.Minimum = 1;
            progressBar.Maximum = (EndDate - StartDate).Days + 1;
            progressBar.Value = 1;

            for (int i = 1; i < progressBar.Maximum; i++)
            {
                GetData(CurrentDate.AddDays(i - 1), FillWeekEnd.ProcessWeekend);

                progressBar.Value = i + 1;
            }
        }

        public DateTime CurrentDate
        {
            get;
            set;
        }

        private void GetData(DateTime currentDate, FillWeekEnd processWeekEnd)
        {
            SystemLog.Write("Data receiving started...");

            //TODO: Move to parallel thread
            ReceiveDataWhenFilesExists(currentDate, processWeekEnd);
        }

        private void ReceiveDataWhenFilesExists(DateTime currentDate, FillWeekEnd processWeekEnd)
        {
            if (FilesExists())
            {
                ReceiveDataWithWeekends(currentDate, processWeekEnd);
            }
            else
            {
                SystemLog.Write("Files does not exists", EventLogEntryType.Error);
            }
        }

        private void ReceiveDataWithWeekends(DateTime currentDate, FillWeekEnd processWeekEnd)
        {
            if ((IsMonday(currentDate)) || (IsNeedProcessWeekend(processWeekEnd)))
            {
                ReceiveAndSaveDataBy(currentDate);
            }
            else
            {
                ReceiveAndSaveDataForDeltaDaysBy(currentDate, 3);
            }
        }

        private void ReceiveAndSaveDataForDeltaDaysBy(DateTime currentDate, int DeltaDays)
        {
            for (int i = 0; i < DeltaDays; i++)
            {
                DateTime workDate = currentDate.AddDays(-i);

                Hef3Data2 data = ReceiveDataOnDay(workDate);

                SaveDataToDBWithErrorLogging(workDate, data);
            }

            SystemLog.Write("Complete.");
        }

        private static Hef3Data2 ReceiveDataOnDay(DateTime workDate)
        {
            SystemLog.Write(String.Format("Reading data for {0}...", workDate.ToShortDateString()));

            Hef3Data2 data = null;
            try
            {
                data = ExcelDataReceiver.Instance.ReceiveData(
                    workDate
                );
            }
            catch (Exception e1)
            {
                SystemLog.Write("Возникла ошибка при получении данных:" + e1.Message + "\n" + e1.StackTrace, EventLogEntryType.Error);
            }

            return data;
        }

        private void ReceiveAndSaveDataBy(DateTime currentDate)
        {
            Hef3Data2 data = ReceiveDataOnDay(currentDate);

            SaveDataToDBWithErrorLogging(currentDate, data);

            SystemLog.Write("Complete.");
        }

        private void SaveDataToDBWithErrorLogging(DateTime currentDate, Hef3Data2 data)
        {
            if (null != data)
                try
                {
                    SystemLog.Write("Saving data...");

                    SaveDataToDB(data, currentDate);
                }
                catch (System.Exception ex1)
                {
                    SystemLog.Write("Возникла ошибка сохранения данных:" + ex1.Message + "\n" + ex1.StackTrace, EventLogEntryType.Error);
                }
        }

        private static bool IsNeedProcessWeekend(FillWeekEnd processWeekEnd)
        {
            return processWeekEnd == FillWeekEnd.Ignore;
        }

        private static bool IsMonday(DateTime currentDate)
        {
            return currentDate.DayOfWeek != DayOfWeek.Monday;
        }

        private void SaveDataToDB(Hef3Data2 data, DateTime currentDate)
        {
            Hef3DBDataStorer.Instance.CurrentDate = currentDate;

            Hef3DBDataStorer.Instance.SaveDataToDB(data);
        }

        private MySqlConnection m_connection = null;

        public MySqlConnection Connection
        {
            get
            {
                if (null == m_connection)
                {
                    PrepareConnection();
                }

                return m_connection;
            }
        }

        private void PrepareConnection()
        {
            m_connection = new MySqlConnection(GetConnectionString());
            m_connection.Open();
        }

        private static string GetConnectionString()
        {
            MySqlConnectionStringBuilder sb = new MySqlConnectionStringBuilder();

            SetConnectionStringServer(sb);

            SetConnectionStringUser(sb);

            return sb.ToString();
        }

        private static void SetConnectionStringUser(MySqlConnectionStringBuilder sb)
        {
            sb.UserID = Settings.Default.UserID;
            sb.Password = Settings.Default.Password;
        }

        private static void SetConnectionStringServer(MySqlConnectionStringBuilder sb)
        {
            sb.Server = Settings.Default.Server;
            sb.Database = Settings.Default.Database;
        }

        private bool FilesExists()
        {
            if (!File.Exists(GetBoilerFileName()))
            {
                SystemLog.Write("File not found " + GetBoilerFileName(), EventLogEntryType.Warning);
                return false;
            }

            if (!File.Exists(GetTurbineFileName()))
            {
                SystemLog.Write("File not found " + GetTurbineFileName(), EventLogEntryType.Warning);
                return false;
            }

            return true;
        }

        private static string GetTurbineFileName()
        {
            return Settings.Default.Path + Settings.Default.TurbineFile;
        }

        private static string GetBoilerFileName()
        {
            return Settings.Default.Path + Settings.Default.BoilerFile;
        }

        #region Члены IDisposable

        void IDisposable.Dispose()
        {
            SystemLog.OnWrite -= new OnWriteEventHandler(SystemLog_OnWrite);
        }

        #endregion Члены IDisposable

        private void Hef3ScanerMainForm_Load(object sender, EventArgs e)
        {
            if (ForceExecution)
            {
                GetDataForPeriod();

                if (QuitAfterExecution)
                    System.Windows.Forms.Application.Exit();
            }
        }
    }
}