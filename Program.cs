﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace Hef3MS.Scaner.XLS
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            SystemLog.Source = "Hef3MS";
            SystemLog.LogName = "ScanerXLS";

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            SystemLog.Write("Application started", EventLogEntryType.Information);

            Hef3ScanerMainForm form = new Hef3ScanerMainForm();

            bool ForceExecution = false;
            bool QuitAfterExecution = false;

            for (int i = 0; i < args.Length; i++)
            {
                if (((args[i] == "-f") || (args[i] == "-F")) || ((args[i] == "/f") || (args[i] == "/F")))
                {
                    ForceExecution = true;
                    continue;
                }

                if (((args[i] == "-q") || (args[i] == "-Q")) || ((args[i] == "/q") || (args[i] == "/Q")))
                {
                    QuitAfterExecution = true;
                    continue;
                }
            }

            form.ForceExecution = ForceExecution;
            form.QuitAfterExecution = QuitAfterExecution;

            Application.Run(form);

            form.Dispose();

            SystemLog.Write("Application finished", EventLogEntryType.Information);
        }
    }
}