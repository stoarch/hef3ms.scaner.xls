﻿
using System.Collections.Generic;
using System;

namespace Hef3MS.Sensors
{

    //TODO: Think how SensorValue and Sensor can be merged (parallel hierarhy)
    public class SensorValue
    {
        public SensorValue(string aCaption, double aValue)
        {
            Caption = aCaption;
            Value = aValue;
        }

        public string Caption { get; set; }
        public double Value { get; set; }
    }

    public class Sensor
    {
        public Sensor()
        {
        }

        public Sensor(string aCaption)
        {
            Caption = aCaption;
        }

        public string Caption { get; set; }
        public DateTime DateReceived { get; set; }

        public Guid UID { get; protected set; }
    }

    public sealed class SensorUID
    {
        public static Guid ComplexSensor { get{ return new Guid("c493022a-a0e1-4303-86f6-3d755072e76a"); } }
        public static Guid MultiValueSensor { get{ return new Guid("54692812-dd2c-45ac-a78f-cfa5bac4bfce"); } }
        public static Guid SingleValueSensor { get{ return new Guid("f14df798-419f-4d4a-abeb-445902b14d0a"); } }

        public static Guid Temperature { get{ return new Guid("aa78314d-8ab2-4b14-b69b-cf24ba5409c1"); } }
        public static Guid Pressure { get{ return new Guid("add4f19c-b8a0-46f3-ad1a-30b1ecd96af0"); } }
        public static Guid FlowRate { get{ return new Guid("9fc3783a-9378-473e-ae70-3160aa8228ad"); } }

        public static Guid MediumSensor { get{ return new Guid("f14df798-419f-4d4a-abeb-445902b14d0a"); } }
    }


    //TODO: SensorValue must be used here
    public class SingleValueSensor : Sensor
    {
        public SingleValueSensor()
        {
            UID = SensorUID.SingleValueSensor;
        }

        public double Value { get; set; }
    }

    //TODO: Think how MultiValueSensor and ComplexSensor can be merged (parallel hierarchies)
    public class MultiValueSensor : Sensor
    {
        public MultiValueSensor()
        {
            Values = new List<SensorValue>();

            UID = SensorUID.MultiValueSensor;
        }

        public List<SensorValue> Values { get; set; }
    }

    public class ComplexSensor : Sensor
    {
        public ComplexSensor()
        {
            Sensors = new List<Sensor>();

            UID = SensorUID.ComplexSensor;
        }

        public List<Sensor> Sensors { get; set; }

        public Sensor this[Guid uid]
        {
            get
            {
                return Sensors.Find( (sensor) => (sensor.UID.Equals( uid )) );
            }
        }

        public Sensor this[string Caption]
        {
            get
            {
                return Sensors.Find((sensor) => (Caption == sensor.Caption));
            }
        }

    }

    public sealed class SensorParameterCaption
    {
        public static string Actual { get { return "Actual"; } }
        public static string ByDevice { get { return "ByDevice"; } }

        public static string Temperature { get { return "Temperature"; } }
        public static string Pressure { get { return "Pressure"; } }
        public static string FlowRate { get { return "FlowRate"; } }
    }

    public sealed class SensorCaption
    {
        public static string Temperature { get { return "Temperature"; } }
        public static string Pressure { get { return "Pressure"; } }
        public static string FlowRate { get { return "FlowRate"; } }

        public static string FeedWater { get { return "Feed water"; } }
        public static string Steam { get { return "Steam"; } }
    }

    public sealed class SensorKindCaption
    {
        public static string Incoming { get { return "Incoming"; } }
        public static string Outgoing { get { return "Outgoing"; } }
    }

    public class TemperatureSensor : SingleValueSensor
    {
        public TemperatureSensor()
        {
            Caption = SensorParameterCaption.Temperature;

            UID = SensorUID.Temperature;
        }

        public TemperatureSensor( string header )
        {
            Caption = header + SensorParameterCaption.Temperature;
        }
    }

    public class PressureSensor : SingleValueSensor
    {
        public PressureSensor()
        {
            Caption = SensorParameterCaption.Pressure;

            UID = SensorUID.Pressure;
        }
    }

    public class FlowRateSensor : MultiValueSensor
    {
        public FlowRateSensor()
        {
            UID = SensorUID.FlowRate;

            Caption = SensorParameterCaption.FlowRate;

            Values.Add(new SensorValue(SensorParameterCaption.Actual, 0));
            Values.Add(new SensorValue(SensorParameterCaption.ByDevice, 0));
        }

        public SensorValue this[string Caption]
        {
            get
            {
                return Values.Find((value) => (value.Caption == Caption));
            }
        }

        public SensorValue Actual
        {
            get
            {
                return this[SensorParameterCaption.Actual];
            }
        }

        public SensorValue ByDevice
        {
            get
            {
                return this[SensorParameterCaption.ByDevice];
            }
        }


    }

    public class MediumSensor : ComplexSensor
    {
        public MediumSensor()
        {
            UID = SensorUID.MediumSensor;

            Sensors.Add(new TemperatureSensor());
            Sensors.Add(new PressureSensor());
            Sensors.Add(new FlowRateSensor());
        }

        public TemperatureSensor Temperature
        {
            get
            {
                return (TemperatureSensor)this[SensorParameterCaption.Temperature];
            }
        }

        public PressureSensor Pressure
        {
            get
            {
                return (PressureSensor)this[SensorParameterCaption.Pressure];
            }
        }

        public FlowRateSensor FlowRate
        {
            get
            {
                return (FlowRateSensor)this[SensorParameterCaption.FlowRate];
            }
        }

    }

}