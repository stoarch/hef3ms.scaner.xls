﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Hef3MS.Scaner.XLS
{
    public delegate void OnWriteEventHandler( string message, EventLogEntryType kind );

    public sealed class SystemLog
    {
        public static String Source { get; set; }
        public static String LogName { get; set; }

        public static void Write(string message)
        {
            Write(message, EventLogEntryType.Information);
        }

        private const string c_information = "Information";

        public static void Write(string message, EventLogEntryType kind )
        {
            if (!EventLog.SourceExists(Source))
            {
                EventLog.CreateEventSource(Source, LogName);
            }

            EventLog.WriteEntry(Source, message, kind);

            Fire_OnWrite(message, kind);
        }

        private static void Fire_OnWrite(string message, EventLogEntryType kind)
        {
            if (null != OnWrite)
            {
                OnWrite(message, kind);
            }
        }

        public static event OnWriteEventHandler OnWrite; 
    }
}
