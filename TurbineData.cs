﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Hef3MS.Sensors;
using Hef3MS.Sensors.HefSensors;

namespace Hef3MS.Scaner.XLS
{
    public class TurbineData
    {
        public TurbineData()
        {
            Temperature = new TemperatureSensor();
            Pressure = new PressureSensor();
            FlowRate = new FlowRateSensor();

            ExhaustSteam = new SteamSensor();
            POSteam = new SteamSensor();
            TOSteam = new SteamSensor();

            CirculateWater = new CirculateWaterSensor(); 
        }

        public TemperatureSensor Temperature { get; set; }
        public PressureSensor Pressure { get; set; }
        public FlowRateSensor FlowRate { get; set; }

        public SteamSensor ExhaustSteam { get; set; }
        public SteamSensor POSteam { get; set; } //factory needs
        public SteamSensor TOSteam { get; set; } //cogeneration picking

        public CirculateWaterSensor CirculateWater { get; set; }

        public int WorkingHours { get; set; }
        public int StartStopCount { get; set; }
    }
}
